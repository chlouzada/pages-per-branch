# pages-per-branch

Exemplo de repositório hospedando duas branches simultaneamente no GitLab Pages.

[Main](https://chlouzada.gitlab.io/pages-per-branch/)

[Staging](https://chlouzada.gitlab.io/pages-per-branch/staging/)




## Cache

Utiliza-se um cache da pasta public e adiciona uma `key` fixa para ser compartilhada por todas as branches.

```
cache:
  key: one-key-to-rule-them-all
  paths:
  - public
```

Obs.: Se uma das branches for protegida a outra também deve ser, pois o GitLab adiciona o sufixo a key  `protected`. O contrário também é valido pois é adicionado `non_protected` como sufixo.

## Variables

É utilizado as variaveis abaixo para determinar se a branch que está rodando a pipeline é a main, a fim de separar os arquivos gerados pelo build.

* CI_COMMIT_REF_SLUG
* CI_DEFAULT_BRANCH


## GitLab CI

Arquivo final `gitlab-ci.yml`

~~~yml
image: node:16.5.0

cache:
  key: one-key-to-rule-them-all
  paths:
  - public

pages:
  stage: deploy
  script:
  - dir="$CI_COMMIT_REF_SLUG"
  - if [ "$CI_COMMIT_REF_SLUG" == "$CI_DEFAULT_BRANCH" ]; then dir=""; fi;
  - dir="public/$dir"
  - echo "Deploying to $dir"
  - npm install
  - npm run build
  - mkdir -p $dir
  - cp -a dist/. $dir
  artifacts:
    paths:
    - public
  only:
  - staging
  - main
~~~